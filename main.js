// Import the functions you need from the SDKs you need

import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } from
"https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

  // Your web app's Firebase configuration
  const firebaseConfig = {
        apiKey: "AIzaSyC27ynbHjFgyFc6ugmNr1lB9tdQIAE_Wog",
        authDomain: "proyectowebfinal-dcbe0.firebaseapp.com",
        databaseURL: "https://proyectowebfinal-dcbe0-default-rtdb.firebaseio.com",
        projectId: "proyectowebfinal-dcbe0",
        storageBucket: "proyectowebfinal-dcbe0.appspot.com",
        messagingSenderId: "634292045226",
        appId: "1:634292045226:web:2c63e3414933a747bb57d3"
  };
// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

// Variables para manejo de la imagen

const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');

var numSerie = 0;
var marca ="";
var color ="";
var descripcion ="";
var urlImag ="";


function leerInputs(){
    numSerie = document.getElementById('txtNumSerie').value;
    marca = document.getElementById('txtMarca').value;
    color =  document.getElementById('txtColor').value;
    descripcion = document.getElementById('txtDescripcion').value;
    urlImag = document.getElementById('txtUrl').value;
}
function mostrarMensaje(mensaje){
    var mensajeElement = document.getElementById('mensaje')
    mensajeElement.textContent= mensaje;
    mensajeElement.style.display= 'block';
    setTimeout(()=>{
    mensajeElement.style.display ='none'},1000);
}



function limpiarInputs() {
    document.getElementById('txtNumSerie').value = '';
    document.getElementById('txtMarca').value = '';
    document.getElementById('txtColor').value = '';
    document.getElementById('txtDescripcion').value = '';
    document.getElementById('txtUrl').value = '';
}

function escribirInputs() {

    document.getElementById('txtColor').value = color;
    document.getElementById('txtMarca').value = marca;
    document.getElementById('txtDescripcion').value = descripcion;
    document.getElementById('txtUrl').value = urlImag;
}


function buscarProducto()
{
    let numSerie = document.getElementById('txtNumSerie').value.trim();
    if(numSerie=== "") {
        mostrarMensaje("No se ingreso Num Serie");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Labiales/' + numSerie)).then((snapshot) =>{
        if(snapshot.exists()) {
            marca = snapshot.val().marca;
            color = snapshot.val().color;
            descripcion = snapshot.val().descripcion;
            urlImag = snapshot.val().urlImag;
            escribirInputs();
        } else {
            limpiarInputs();
            mostrarMensaje("El Producto con Codigo" + numSerie + "No existe.");
        }
    });
}

const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);
btnBuscar.addEventListener('click', buscarProducto);

function insertarProducto(){
    alert("ingrese a add db");
    leerInputs();

    if(numSerie==="" || marca==="" || color==="" || descripcion===""){
        mostrarMensaje("faltaron datos por capturar");
        return;
    }

    set(
         refS(db,'Labiales/' + numSerie),
         {
            numSerie:numSerie,
            marca:marca,
            color:color,
            descripcion:descripcion,
            urlImag:urlImag
         }
    ).then(()=>{

        alert("Se agrego con exito");
    }).catch((error)=>{
        alert("Ocurrio un error")
 })
}

Listarproductos()
function Listarproductos() {
    const dbRef = refS(db, 'Labiales');
    const tabla = document.getElementById('tablaProductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';
    onValue(dbRef,(snapshot) => {
        snapshot.forEach((childSnapshot)=> {
            const childKey = childSnapshot.key;

            const data = childSnapshot.val();
            var fila = document.createElement('tr');

            var celdaCodigo = document.createElement('td');
            celdaCodigo.textContent = childKey;
            fila.appendChild(celdaCodigo);

            var celdaNombre = document.createElement('td');
            celdaNombre.textContent = data.marca;
            fila.appendChild(celdaNombre);

            var celdaPrecio = document.createElement('td');
            celdaPrecio.textContent = data.color;
            fila.appendChild(celdaPrecio);

            var celdaCantidad = document.createElement('td');
            celdaCantidad.textContent = data.descripcion;
            fila.appendChild(celdaCantidad);

            var celdaImagen = document.createElement('td');
            var imagen = document.createElement('img');
            imagen.src = data.urlImg;
            imagen.width = 100;
            celdaImagen.appendChild(imagen);
            fila.appendChild(celdaImagen);
            tbody.appendChild(fila);
        });
    }, { onlyOnce: true});
}


function actualizarLabiales() {
    leerInputs();
    if (numSerie === "" || marca === "" || color === "" || descripcion === "") {
        mostrarMensaje("Favor de capturar toda la informacion.");
        return;
    }
    alert("actualizar");
    update(refS(db, 'Labiales/' + numSerie), {
        numSerie:numSerie,
        marca: marca,
        color: color,
        descripcion: descripcion,
        urlImg: urlImag
    }).then(() => {
        mostrarMensaje("Se actualizó con éxito.");
        limpiarInputs();
        Listarproductos();
    }).catch((error) => {
        mostrarMensaje("Ocurrió un error: " + error);
    });
}



function eliminarLabiales() {
    let numSerie= document.getElementById('txtNumSerie').value.trim();
    if (numSerie === "") {
        mostrarMensaje("No se ingreso un Codigo válido.");
        return;
    }
    const dbref = refS(db);
    get(child(dbref, 'Labiales/' + numSerie)).then((snapshot) => {

        if (snapshot.exists()) {
            remove(refS(db, 'Labiales/' + numSerie))
            .then(() => {
                mostrarMensaje("Producto eliminado con éxito.");
                limpiarInputs();
                Listarproductos();
            })
            .catch((error) => {
                mostrarMensaje("Ocurrió un error al eliminar el producto:  " + error);
            });
        }else {
            limpiarInputs();
            mostrarMensaje("El producto don ID " + numSerie + "no existe.");

        }
    });
    Listarproductos();
}

uploadButton.addEventListener('click', (event) => {
    event.preventDefault();
    const file = imageInput.files[0];
     
        if (file) {
            const storageRef = ref(storage, file.name);
            const uploadTask = uploadBytesResumable(storageRef, file);
            uploadTask.on('state_changed', (snapshot) => {
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                progressDiv.textContent = 'Progreso: ' + progress.toFixed(2) + '%';
            }, (error) => {
                console.error(error);
                
            }, () => {
                getDownloadURL(uploadTask.snapshot.ref).then((downloadUrl) => {
                    txtUrlInput.value = downloadUrl;
                    setTimeout(() => {
                        progressDiv.textContent = '';
                    }, 500);
                }).catch((error) => {
                    console.error(error);

                })
            });
        }
});

const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarLabiales);

const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', eliminarLabiales);